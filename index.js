const fs = require('fs');
const swifer = require('./pkg');
const util = require('util');

const data = fs.readFileSync(process.argv[2]);
const buf = swifer.decompress_swf(data);
const swf = swifer.parse_swf(buf);

const abc = swf.find_abc();

console.log(abc.instances.length);

for (let i = 0; i < abc.instances.length; i++) {
  console.log(abc.instances[i].name);
}

const strings = abc.constant_pool.strings.map(x => x.replace(/KARKAT/g, 'CRAB'));
abc.constant_pool.strings = strings;

const new_buf = swf.write_with_abc(abc);

fs.writeFileSync(process.argv[3], new_buf);
