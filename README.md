# swifer

Experimental SWF parser/writer for JavaScript, powered by [ruffle's `swf` crate](https://github.com/ruffle-rs/ruffle/tree/master/swf).

## Example
```js
const swf_buf = swifer.decompress_swf(data);
const swf = swifer.parse_swf(swf_buf);

const abc = swf.find_abc();

const strings = abc.constant_pool.strings.map(x => x.replace(/KARKAT/g, 'CRAB'));
abc.constant_pool.strings = strings;

const new_data = swf.write_with_abc(abc);
```
