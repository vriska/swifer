use js_sys::{Object, Proxy};
use paste::paste;
use self_cell::self_cell;
use std::cell::{Ref, RefCell, RefMut};
use std::ops::{Deref, DerefMut};
use std::rc::Rc;
use swf::Swf as RealSwf;
use wasm_bindgen::{prelude::*, JsCast};

#[wasm_bindgen]
extern "C" {
    #[wasm_bindgen(typescript_type = "number[]")]
    pub type NumberArray;

    #[wasm_bindgen(typescript_type = "string[]")]
    pub type StringArray;
}

#[wasm_bindgen]
pub struct SwfBuf(swf::SwfBuf);

self_cell! {
    pub struct SwfData {
        owner: SwfBuf,

        #[covariant]
        dependent: RealSwf,
    }
}

#[wasm_bindgen]
pub struct Swf(SwfData);

#[wasm_bindgen]
#[derive(Clone)]
pub struct AbcFile(Rc<RefCell<swf::avm2::types::AbcFile>>);

macro_rules! projection {
    (
        $(#[$attrs:meta])*
        $vis:vis struct $proj:ident {
            inner: $inner:ty,
            target: $target:ty = |$inner_name:ident| $expr:expr,
        }
    ) => {
        projection! {
            $(#[$attrs])* $vis struct $proj {
                inner: $inner,
                target: $target = |self, $inner_name| $expr,
            }
        }
    };

    (
        $(#[$attrs:meta])*
        $vis:vis struct $proj:ident {
            inner: $inner:ty,
            target: $target:ty = |$self:ident, $inner_name:ident| $expr:expr,
            $($field_name:ident: $field_type:ty,)*
        }
    ) => {
        $(#[$attrs])*
        $vis struct $proj {
            inner: $inner,
            $($field_name: $field_type,)*
        }

        impl $proj {
            pub fn borrow(&$self) -> impl Deref<Target = $target> + '_ {
                Ref::map($self.inner.0.borrow(), |$inner_name| &$expr)
            }

            pub fn borrow_mut(&$self) -> impl DerefMut<Target = $target> + '_ {
                RefMut::map($self.inner.0.borrow_mut(), |$inner_name| &mut $expr)
            }
        }
    };

    (
        $(#[$attrs:meta])*
        $vis:vis struct $proj:ident {
            inner: $inner:ty,
            elem: $elem:ident = |$inner_name:ident| $expr:expr,
        }
    ) => {
        $(#[$attrs])*
        $vis struct $proj {
            inner: $inner,
        }

        impl From<$proj> for JsValue {
            fn from(proj: $proj) -> Self {
                #[wasm_bindgen]
                struct Handler {
                    inner: $inner,
                }

                #[wasm_bindgen]
                impl Handler {
                    #[allow(dead_code)]
                    pub fn get(&self, _target: JsValue, prop: &str, _receiver: JsValue) -> JsValue {
                        if prop == "length" {
                            let $inner_name = self.inner.0.borrow();
                            let slice = $expr;
                            slice.len().into()
                        } else if let Ok(idx) = prop.parse::<usize>() {
                            $elem {
                                inner: self.inner.clone(),
                                idx,
                            }
                            .into()
                        } else {
                            JsValue::UNDEFINED
                        }
                    }
                }

                let handler = Handler { inner: proj.inner };
                let target = Object::new();

                let proxy = Proxy::new(&*target, JsValue::from(handler).dyn_ref().unwrap());

                proxy.into()
            }
        }
    };
}

macro_rules! projections {
    () => {};

    ($(#[$attrs:meta])* $vis:vis struct $proj:ident $body:tt $($rest:tt)*) => {
        projection!($(#[$attrs])* $vis struct $proj $body);
        projections!($($rest)*);
    };
}

projections! {
    #[wasm_bindgen]
    pub struct ConstantPool {
        inner: AbcFile,
        target: swf::avm2::types::ConstantPool = |abc| abc.constant_pool,
    }

    #[wasm_bindgen]
    pub struct Instance {
        inner: AbcFile,
        target: swf::avm2::types::Instance = |self, abc| abc.instances[self.idx],
        idx: usize,
    }

    pub struct InstanceArray {
        inner: AbcFile,
        elem: Instance = |abc| &abc.instances,
    }
}

#[wasm_bindgen]
pub fn decompress_swf(input: &[u8]) -> Result<SwfBuf, JsValue> {
    let inner = swf::decompress_swf(input).map_err(|x| x.to_string())?;

    Ok(SwfBuf(inner))
}

#[wasm_bindgen]
pub fn parse_swf(swf_buf: SwfBuf) -> Result<Swf, JsValue> {
    Ok(Swf(SwfData::try_new(swf_buf, |swf_buf| {
        swf::parse_swf(&swf_buf.0).map_err(|x| JsValue::from(x.to_string()))
    })?))
}

#[wasm_bindgen]
impl Swf {
    pub fn find_abc(&self) -> Result<Option<AbcFile>, JsValue> {
        self.0
            .borrow_dependent()
            .tags
            .iter()
            .find_map(|x| {
                if let swf::Tag::DoAbc(swf::DoAbc { data, .. }) = x {
                    Some(data)
                } else {
                    None
                }
            })
            .map(|data| {
                let mut reader = swf::avm2::read::Reader::new(data);
                let inner = reader.read().map_err(|x| JsValue::from(x.to_string()))?;
                Ok(AbcFile(Rc::new(RefCell::new(inner))))
            })
            .transpose()
    }

    pub fn write_with_abc(mut self, file: AbcFile) -> Result<Box<[u8]>, JsValue> {
        self.0.with_dependent_mut(|_, inner| {
            let mut abc = Vec::new();
            let mut abc_writer = swf::avm2::write::Writer::new(&mut abc);
            abc_writer
                .write(file.into_inner())
                .map_err(|x| x.to_string())?;

            let mut tags = Vec::new();
            for tag in inner.tags.drain(..) {
                if let swf::Tag::DoAbc(do_abc) = tag {
                    let mut new = do_abc.clone();
                    new.data = &abc;
                    tags.push(swf::Tag::DoAbc(new));
                } else {
                    tags.push(tag);
                }
            }

            let mut data = Vec::new();
            swf::write::write_swf(inner.header.swf_header(), &tags, &mut data)
                .map_err(|x| x.to_string())?;

            Ok(data.into_boxed_slice())
        })
    }
}

impl AbcFile {
    pub fn into_inner(self) -> swf::avm2::types::AbcFile {
        match Rc::try_unwrap(self.0) {
            Ok(cell) => cell.into_inner(),
            Err(rc) => rc.borrow().clone(),
        }
    }
}

#[wasm_bindgen]
impl AbcFile {
    #[wasm_bindgen(getter)]
    pub fn constant_pool(&self) -> ConstantPool {
        ConstantPool {
            inner: self.clone(),
        }
    }

    #[wasm_bindgen(getter)]
    pub fn instances(&self) -> JsValue {
        InstanceArray {
            inner: self.clone(),
        }
        .into()
    }
}

#[wasm_bindgen]
impl Instance {
    #[wasm_bindgen(getter)]
    pub fn name(&self) -> String {
        let this = self.borrow();
        let idx = &this.name;
        let abc = self.inner.0.borrow();
        let multiname = &abc.constant_pool.multinames[idx.0 as usize];

        match multiname {
            swf::avm2::types::Multiname::QName { name, .. } => {
                abc.constant_pool.strings[name.0 as usize].clone()
            }
            _ => unimplemented!(),
        }
    }
}

macro_rules! getter_setter {
    ($type:ty, $($field:ident: $field_ty:ty),*) => {
        paste! {
            #[wasm_bindgen]
            impl $type {
                $(
                    #[wasm_bindgen(getter)]
                    pub fn $field(&self) -> $field_ty {
                        JsValue::from_serde(&self.borrow().$field).unwrap().into()
                    }

                    #[wasm_bindgen(setter)]
                    pub fn [<set_ $field>](&self, $field: $field_ty) -> Result<(), JsValue> {
                        let val = JsValue::from($field).into_serde().map_err(|x| x.to_string())?;

                        self.borrow_mut().$field = val;

                        Ok(())
                    }
                )*
            }
        }
    };
}

getter_setter!(
    ConstantPool,
    ints: NumberArray,
    uints: NumberArray,
    doubles: NumberArray,
    strings: StringArray
);
